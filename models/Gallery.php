<?
    class Gallery {
        public static function getList($filename, $device) {
            $size_list = Size::getSizeList();
            require ROOT."/libs/generateImg.php";
            return self::generateList($size_list, $filename, $device);
        }

        public static function generateList($size_list, $filename, $device) {
            foreach ($size_list as $key => $value) {
                $newName = explode('.', $filename);
                $newName = $newName[0].'_'.$value['width'].'x'.$value['height'].'.'.$newName[1];
                if (!file_exists(ROOT.'/cache/'.$newName)) {
                    generateImg(
                        ROOT.'/gallery/'.$filename,
                        ROOT.'/cache/'.$newName,
                        $value['width'], $value['height']
                    );
                }
                if (
                    (
                        $device == 'pc' &&
                        $value['name'] != 'mic'
                    ) || (
                        $device == 'mobile' &&
                        $value['name'] != 'big'
                    ) || (
                        $device != 'pc' &&
                        $device != 'mobile'
                    )
                ) {
                    $result[] = array(
                        'name' => explode('.', $filename)[0],
                        'size' => $value['name'],
                    );
                }
            }
            return $result;
        }
    }