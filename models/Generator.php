<?
    $name = $_GET['name'];
    $size = Size::getSizeByName(
        $_GET['size']
    );
    $cache = scandir(ROOT.'/'.'gallery/');
    foreach ($cache as $key => $value) {
        if (strripos($value, $name) !== false) {
            $value = explode('.', $value);
            $fileName = ROOT.'/cache/'.$value[0].'_'.$size['width'].'x'.$size['height'].'.'.$value[1];
            if (file_exists($fileName)) {
                $url = $fileName;
                header('Content-type: image/jpeg');
                imagejpeg(
                    imagecreatefromjpeg(
                        $url
                    )
                );
                break;
                return true;
            }
        }
    }

    echo 'Такого изображения нету';