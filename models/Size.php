<?
    class Size {

        public static function getSizeList() {
            global $db;
            $query = "
                SELECT
                    *
                FROM
                    `size_list`
            ";
            return $db->query($query);
        }

        public static function getSizeByName($name) {
            $sizeList = self::getSizeList();
            foreach ($sizeList as $key => $value) {
                if ($value['name'] == $name) {
                    return $value;
                }
            }
            return false;
        }

    }