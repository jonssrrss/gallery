<div class="photo-slider">
    <? foreach ($gallery as $key => $value) { ?>
        
        <div>
            <div class="sl">
                <a data-fancybox="images" href="/generator.php?name=<?=$value['name'];?>&size=<?=$value['size'];?>" tabindex="0" style="cursor: pointer;" data-idphoto="1">
                    <img src="/generator.php?name=<?=$value['name'];?>&size=<?=$value['size'];?>" alt="">
                </a>
            </div>
        </div>

    <? } ?>
</div>

<script>
$('.photo-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 6,
});
</script>