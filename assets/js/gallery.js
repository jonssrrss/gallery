var gallery = {

    getPreview: function() {
        $.get(
            '/preview/get',
            {},
            function (responseHtml) {
                $('#result-preview').html(responseHtml);
            }
        );
    },

    getGallery: function(name) {
        var name = name;
        var device = '';
        if (window.innerWidth <= 400) {
            device = 'mobile';
        } else if (window.innerWidth >= 1200) {
            device = 'pc';
        }
        $.post(
            '/gallery.php',
            {
                name: name,
                device: device,
            },
            function (responseHtml) {
                $('#result-gallery').html(responseHtml);
            }
        );
    }

}

$(function(){
    gallery.getPreview();
});