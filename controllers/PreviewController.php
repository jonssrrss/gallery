<?
	class PreviewController {
		public static function actionGet() {
            require_once(ROOT.'/models/Preview.php');
			$gallery = Preview::getPreviewFileList();
			require_once(ROOT.'/views/preview/index.php');
		}
	}