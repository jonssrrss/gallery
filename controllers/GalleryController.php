<?
	class GalleryController {
		public static function actionGet() {
            require_once(ROOT.'/models/Size.php');
            require_once(ROOT.'/models/Gallery.php');
            $gallery = Gallery::getList(
				$_POST['name'],
				$_POST['device']
			);
			require_once(ROOT.'/views/Gallery/index.php');
		}
	}