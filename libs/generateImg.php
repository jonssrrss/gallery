<?
	/**
	 * @param $sSrcname  Файл исходного изображения
	 * @param $sDstName  Файл уменьшенной копии
	 * @param $iDstW     Максимальная ширина копии
	 * @param $iDstH     Максимальная высота копии
	 */
	function generateImg($sSrcName, $sDstName, $iDstW, $iDstH) {
		// Получим информацию о исходном изображении
		$aImgInfo = getImageSize($sSrcName);

		// Определим тип
		switch ($aImgInfo[2])
		{
			case IMAGETYPE_JPEG:
				$sType = 'jpeg';
			break;

			case IMAGETYPE_PNG:
				$sType = 'png';
			break;
			
			case IMAGETYPE_GIF:
				$sType = 'gif';
			break;

			default:
				// Тип нам не нравится
				return false;
			break;
		}

		if (!function_exists('imagecreatefrom' . $sType)) {
			return false;
		}

		$imgSrc = call_user_func('imagecreatefrom' . $sType, $sSrcName);
		list($iSrcW, $iSrcH) = $aImgInfo;

		// Далее алгоритм определения пропорции
		if (($iSrcW > $iDstW) || ($iSrcH > $iDstH)) {
			if ($iSrcW > $iSrcH) {
				$fK = $iDstW / $iSrcW;
			} else {
				$fK = $iDstH / $iSrcH;
			}
		} else {
			$fK = 1;
		}

		$iNewW = round($iSrcW * $fK);
		$iNewH = round($iSrcH * $fK);

		// Создаем уменьшенную копию
		$imgDst = imagecreatetruecolor($iNewW, $iNewH);
		ImageCopyResampled(
			$imgDst,
			$imgSrc,
			0, 0, 0, 0,
			$iNewW,
			$iNewH,
			$iSrcW,
			$iSrcH
		); // Изменяем размер пропорционально со сглаживанием

		// Сохраняем в $sDstName
		call_user_func_array('image' . $sType, array($imgDst, $sDstName));
		return true;
	}